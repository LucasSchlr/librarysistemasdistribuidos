package br.faccat.engsoft.LibraryManagement.config;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ApiException extends RuntimeException  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpStatus status;
	private String erro;
	
	public ApiException(HttpStatus status, String erro) {
        super(erro);
		this.status = status;
		this.erro = erro;
    }
}