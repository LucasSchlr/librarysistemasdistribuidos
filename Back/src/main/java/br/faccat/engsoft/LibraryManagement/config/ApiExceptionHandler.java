package br.faccat.engsoft.LibraryManagement.config;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(ApiException.class)
	public ResponseEntity<ErroAPI> erroWrongCredentials(ApiException ex, WebRequest request) {
		ErroAPI erro = new ErroAPI();
		System.out.println("ex: "+ex.toString());
		
		erro.setStatus(ex.getStatus().value());
		erro.setErro(ex.getErro());

		return new ResponseEntity<>(erro, ex.getStatus());
	}

}
