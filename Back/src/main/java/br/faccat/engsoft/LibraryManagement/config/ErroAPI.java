package br.faccat.engsoft.LibraryManagement.config;

import lombok.Data;

@Data
public class ErroAPI {
	private Integer status;
	private String erro;
	private String origem;
	private String metodo;
}