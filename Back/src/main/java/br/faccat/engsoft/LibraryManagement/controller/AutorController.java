package br.faccat.engsoft.LibraryManagement.controller;

import java.util.List;

import br.faccat.engsoft.LibraryManagement.service.AutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.faccat.engsoft.LibraryManagement.entity.Autor;

@RestController
public class AutorController {

    @Autowired
    private AutorService autorService;

    @RequestMapping(value = "/autor", method = RequestMethod.GET)
    public List<Autor> Get() {
        return autorService.getAllAutores();
    }

    @RequestMapping(value = "/autor/{id}", method = RequestMethod.GET)
    public ResponseEntity<Autor> GetById(@PathVariable(value = "id") long id) {
        Autor autor = autorService.getAutorById(id);
        return ResponseEntity.ok(autor);
    }

    @RequestMapping(value = "/autor", method =  RequestMethod.POST)
    public ResponseEntity<Autor> Post(@RequestBody Autor autor) {
        return ResponseEntity.ok(autorService.createAutor(autor));
    }

    @RequestMapping(value = "/autor/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Autor> Put(@PathVariable(value = "id") long id, @RequestBody Autor newAutor) {
        return ResponseEntity.ok(autorService.atualizaAutor(id, newAutor));
    }

    @RequestMapping(value = "/autor/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
        autorService.deleteAutor(id);
        return ResponseEntity.noContent().build();
    }
}