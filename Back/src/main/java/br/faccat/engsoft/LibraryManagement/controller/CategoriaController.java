package br.faccat.engsoft.LibraryManagement.controller;

import java.util.List;

import br.faccat.engsoft.LibraryManagement.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.faccat.engsoft.LibraryManagement.entity.Categoria;

@RestController
public class CategoriaController {

    @Autowired
    private CategoriaService categoriaService;

    @RequestMapping(value = "/categoria", method = RequestMethod.GET)
    public ResponseEntity<List<Categoria>> Get() {
        return ResponseEntity.ok(categoriaService.getAllCategoria());
    }

    @RequestMapping(value = "/categoria/{id}", method = RequestMethod.GET)
    public ResponseEntity<Categoria> GetById(@PathVariable(value = "id") long id) {
        return ResponseEntity.ok(categoriaService.getCategoriaById(id));
    }

    @RequestMapping(value = "/categoria", method =  RequestMethod.POST)
    public ResponseEntity<Categoria> Post(@RequestBody Categoria categoria) {
        return ResponseEntity.ok(categoriaService.createCategoria(categoria));
    }

    @RequestMapping(value = "/categoria/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Categoria> Put(@PathVariable(value = "id") long id, @RequestBody Categoria newCategoria) {
        return ResponseEntity.ok(categoriaService.atualizaCategoria(id, newCategoria));
    }

    @RequestMapping(value = "/categoria/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
        categoriaService.deleteCategoria(id);
        return ResponseEntity.noContent().build();
    }
}