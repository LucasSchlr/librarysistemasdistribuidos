package br.faccat.engsoft.LibraryManagement.controller;

import java.util.List;

import br.faccat.engsoft.LibraryManagement.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.faccat.engsoft.LibraryManagement.entity.Cliente;

@RestController
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @RequestMapping(value = "/cliente", method = RequestMethod.GET)
    public List<Cliente> Get() {
        return clienteService.getAllCliente();
    }

    @RequestMapping(value = "/cliente/{id}", method = RequestMethod.GET)
    public ResponseEntity<Cliente> GetById(@PathVariable(value = "id") long id) {
        return ResponseEntity.ok(clienteService.getClienteById(id));
    }

    @RequestMapping(value = "/cliente", method =  RequestMethod.POST)
    public ResponseEntity<Cliente> Post(@RequestBody Cliente cliente) {
        return ResponseEntity.ok(clienteService.createCliente(cliente));
    }

    @RequestMapping(value = "/cliente/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Cliente> Put(@PathVariable(value = "id") long id, @RequestBody Cliente newCliente) {
        return ResponseEntity.ok(clienteService.atualizaCliente(id, newCliente));
    }

    @RequestMapping(value = "/cliente/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
        clienteService.deleteCliente(id);
        return ResponseEntity.noContent().build();
    }
}