package br.faccat.engsoft.LibraryManagement.controller;

import br.faccat.engsoft.LibraryManagement.dto.LivroDto;
import br.faccat.engsoft.LibraryManagement.entity.Livro;
import br.faccat.engsoft.LibraryManagement.service.LivroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class LivroController {

    @Autowired
    private LivroService livroService;

    @RequestMapping(value = "/livro", method = RequestMethod.GET)
    public ResponseEntity<List<Livro>> Get() {
        return ResponseEntity.ok(livroService.getAllLivros());
    }

    @RequestMapping(value = "/livro/{id}", method = RequestMethod.GET)
    public ResponseEntity<Livro> GetById(@PathVariable(value = "id") long id) {
        Optional<Livro> livro = livroService.getLivroById(id);
        return livro.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @RequestMapping(value = "/livro", method =  RequestMethod.POST)
    public ResponseEntity<Livro> Post(@RequestBody LivroDto livroDto) {
        return ResponseEntity.ok(livroService.createLivro(livroDto));
    }

    @RequestMapping(value = "/livro/cliente", method = RequestMethod.GET)
    public ResponseEntity<List<Livro>> getLivrosByNomeCliente(@RequestParam String nomeCliente) {
        List<Livro> livros = livroService.getByNomeCliente(nomeCliente);
        if(livros.isEmpty()) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(livros);
    }

    @RequestMapping(value = "/livro/autor", method = RequestMethod.GET)
    public ResponseEntity<List<Livro>> getLivrosByNomeAutor(@RequestParam String nomeAutor) {
        List<Livro> listLivro = livroService.getLivrosByNomeAutor(nomeAutor);
        if(listLivro.isEmpty()) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(listLivro);
    }

    @RequestMapping(value = "/livro/categoria", method = RequestMethod.GET)
    public ResponseEntity<List<Livro>> getLivrosByNomeCategoria(@RequestParam String nomeCategoria) {
        List<Livro> listLivro = livroService.getLivrosByCategoria(nomeCategoria);
        if(listLivro.isEmpty()) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(listLivro);
    }

    @RequestMapping(value = "/livro/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Livro> Put(@PathVariable(value = "id") long id, @RequestBody LivroDto newLivroDto) {
        Livro livro = livroService.atualizaLivro(id, newLivroDto);
        return ResponseEntity.ok(livro);
    }

    @RequestMapping(value = "/livro/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
        livroService.deleteLivro(id);
        return ResponseEntity.noContent().build();
    }
}