package br.faccat.engsoft.LibraryManagement.controller;

import java.util.List;

import br.faccat.engsoft.LibraryManagement.dto.LivroClienteDto;
import br.faccat.engsoft.LibraryManagement.service.LocacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.faccat.engsoft.LibraryManagement.entity.Locacao;

import javax.validation.Valid;

@RestController
public class LocacaoController {

    @Autowired
    private LocacaoService locacaoService;

    @RequestMapping(value = "/locacao", method = RequestMethod.GET)
    public ResponseEntity<List<Locacao>> Get() {
        return ResponseEntity.ok(locacaoService.getAllLocacao());
    }

    @RequestMapping(value = "/locacao/{id}", method = RequestMethod.GET)
    public ResponseEntity<Locacao> GetById(@PathVariable(value = "id") long id) {
        return ResponseEntity.ok(locacaoService.getLocacaoById(id));
    }

    @RequestMapping(value = "/locacao", method =  RequestMethod.POST)
    public ResponseEntity<Locacao> postLocacao(@Valid @RequestBody LivroClienteDto livroCliente) {
       return ResponseEntity.ok(locacaoService.createLocacao(livroCliente));
    }

    @RequestMapping(value = "/locacao/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Locacao> putDevolucao(@PathVariable(value = "id") long id) {
        return ResponseEntity.ok(locacaoService.devolveLivro(id));
    }

    @RequestMapping(value = "/locacao/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
        locacaoService.deleteLocacao(id);
        return ResponseEntity.noContent().build();
    }
}