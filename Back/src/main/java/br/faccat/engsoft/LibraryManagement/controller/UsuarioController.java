package br.faccat.engsoft.LibraryManagement.controller;

import br.faccat.engsoft.LibraryManagement.dto.LoginDto;
import br.faccat.engsoft.LibraryManagement.entity.Usuario;
import br.faccat.engsoft.LibraryManagement.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/usuario", method = RequestMethod.GET)
    public ResponseEntity<List<Usuario>> Get() {
        return ResponseEntity.ok(usuarioService.getAllUsuario());
    }

    @RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET)
    public ResponseEntity<Usuario> GetById(@PathVariable(value = "id") long id) {
        return ResponseEntity.ok(usuarioService.getUsuarioById(id));
    }

    @PostMapping("/{id}")
    public ResponseEntity<Boolean> login(@RequestBody LoginDto login) {
        return ResponseEntity.ok(usuarioService.login(login));
    }

    @RequestMapping(value = "/usuario", method =  RequestMethod.POST)
    public ResponseEntity<Usuario> Post(@RequestBody Usuario usuario) {
        return ResponseEntity.ok(usuarioService.createUsuario(usuario));
    }

    @RequestMapping(value = "/usuario/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Usuario> Put(@PathVariable(value = "id") long id, @RequestBody Usuario newUsuario) {
       return ResponseEntity.ok(usuarioService.atualizaUsuario(id, newUsuario));
    }

    @RequestMapping(value = "/usuario/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
        usuarioService.deleteUsuario(id);
        return ResponseEntity.noContent().build();
    }
}