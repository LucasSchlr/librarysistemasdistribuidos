package br.faccat.engsoft.LibraryManagement.dto;

import lombok.Data;

@Data
public class LivroClienteDto {

    private Long livroId;
    private Long clienteId;

}
