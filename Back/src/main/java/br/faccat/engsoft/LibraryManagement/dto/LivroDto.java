package br.faccat.engsoft.LibraryManagement.dto;

import lombok.Data;

@Data	
public class LivroDto{
    private String titulo;
    
    private Float preco;
    
    private long autorId;

    private long categoriaId;

    private Integer quantidadeDisponivel;
    
    
}