package br.faccat.engsoft.LibraryManagement.dto;

import lombok.Data;

@Data
public class LocacaoDto {

    private long livroId;
	
    private long clienteId;
    
    private Boolean ativo;
}