package br.faccat.engsoft.LibraryManagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Data	
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Livro{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String titulo;

    @Column(nullable = false)
    private Float preco;

	@ManyToOne(optional = false)
    private Autor autor;

	@ManyToOne(optional = false)
    private Categoria categoria;	

    @Column(nullable = false)
    private Integer quantidadeDisponivel;
    
    
}