package br.faccat.engsoft.LibraryManagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Data
@JsonIgnoreProperties("hibernateLazyInitializer")
public class Locacao {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

	@ManyToOne(optional = false)
    private Livro livro;
	
	@ManyToOne(optional = false)
    private Cliente cliente;
    
    @Column(nullable = false)
    private Boolean ativo;
}