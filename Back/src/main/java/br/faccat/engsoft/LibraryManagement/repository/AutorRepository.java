package br.faccat.engsoft.LibraryManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.faccat.engsoft.LibraryManagement.entity.Autor;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long> {

    @Query(value = "" +
            "select * from autor where nome = ?1", nativeQuery = true)
    Autor getAutorByName(String nomeAutor);
}