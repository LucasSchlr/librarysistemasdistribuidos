package br.faccat.engsoft.LibraryManagement.repository;

import br.faccat.engsoft.LibraryManagement.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    @Query(value = "" +
            "select * from cliente where nome = ?1", nativeQuery = true)
    Cliente getClienteByName(String nomeCliente);
}