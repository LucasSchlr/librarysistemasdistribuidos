package br.faccat.engsoft.LibraryManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.faccat.engsoft.LibraryManagement.entity.Livro;

import java.util.List;

@Repository
public interface LivroRepository extends JpaRepository<Livro, Long> {

    @Query(value = "" +
            "select * from livro liv join locacao loc on liv.id = loc.livro_id where loc.cliente_id = ?1 and loc.ativo = true", nativeQuery = true)
    List<Livro> getLivrosByClienteIdAndAtivoTrue(Long clienteId);

    @Query(value = "" +
            "select * from livro where autor_id = ?1", nativeQuery = true)
    List<Livro> getLivrosByAutorId(Long idAutor);

    @Query(value = "" +
            "select * from livro where categoria_id = ?1", nativeQuery = true)
    List<Livro> getLivrosByCategoriaId(Long categoriaId);
}