package br.faccat.engsoft.LibraryManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.faccat.engsoft.LibraryManagement.entity.Locacao;

import java.util.List;

@Repository
public interface LocacaoRepository extends JpaRepository<Locacao, Long> {

    @Query(value = "" +
            "select * from locacao where cliente_id = ?1", nativeQuery = true)
    List<Locacao> getLocacaoByClienteId(Long clienteId);
}