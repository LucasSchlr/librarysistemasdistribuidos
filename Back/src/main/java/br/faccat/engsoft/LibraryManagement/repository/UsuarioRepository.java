package br.faccat.engsoft.LibraryManagement.repository;

import br.faccat.engsoft.LibraryManagement.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    @Query(value = "" +
            "select * from usuario where login = ?1", nativeQuery = true)
    Usuario getByUsername(String username);

}