package br.faccat.engsoft.LibraryManagement.service;

import br.faccat.engsoft.LibraryManagement.config.ApiException;
import br.faccat.engsoft.LibraryManagement.entity.Autor;
import br.faccat.engsoft.LibraryManagement.repository.AutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutorService {

    @Autowired
    public AutorRepository autorRepository;

    public List<Autor> getAllAutores() {
        return autorRepository.findAll();
    }

    public Autor getAutorById(long id) {
        Autor autor = autorRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Autor com id %d não encontrado.", id)
        	));

        return autor;
    }

    public Autor createAutor(Autor autor) {
        return autorRepository.save(autor);
    }

    public Autor atualizaAutor(long id, Autor newAutor) {
        Autor oldAutor = autorRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Autor com id %d não encontrado.", id)
        	));

        Autor autor = oldAutor;
        autor.setNome(newAutor.getNome());
        autorRepository.save(autor);
        return autor;
    }

    public void deleteAutor(long id) {
        Autor autor = autorRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Autor com id %d não encontrado.", id)
        	));

        autorRepository.delete(autor);
    }
}
