package br.faccat.engsoft.LibraryManagement.service;

import br.faccat.engsoft.LibraryManagement.config.ApiException;
import br.faccat.engsoft.LibraryManagement.entity.Categoria;
import br.faccat.engsoft.LibraryManagement.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    public List<Categoria> getAllCategoria() {
        return categoriaRepository.findAll();
    }

    public Categoria getCategoriaById(long id) {    	
    	Categoria categoria = categoriaRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Categoria com id %d não encontrada.", id)
        	));

        return categoria;
    }

    public Categoria createCategoria(Categoria categoria) {
        return categoriaRepository.save(categoria);
    }

    public Categoria atualizaCategoria(long id, Categoria newCategoria) {
    	Categoria oldCategoria = categoriaRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Categoria com id %d não encontrada.", id)
        	));

    	Categoria categoria = oldCategoria;
    	categoria.setDescricao(newCategoria.getDescricao());
    	return categoriaRepository.save(categoria);
    }

    public void deleteCategoria(long id) {
    	Categoria categoria = categoriaRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Categoria com id %d não encontrada.", id)
        	));    	

    	categoriaRepository.delete(categoria);
    }

}
