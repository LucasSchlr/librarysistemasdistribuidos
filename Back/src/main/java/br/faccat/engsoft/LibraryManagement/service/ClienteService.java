package br.faccat.engsoft.LibraryManagement.service;

import br.faccat.engsoft.LibraryManagement.config.ApiException;
import br.faccat.engsoft.LibraryManagement.entity.Cliente;
import br.faccat.engsoft.LibraryManagement.entity.Locacao;
import br.faccat.engsoft.LibraryManagement.repository.ClienteRepository;
import br.faccat.engsoft.LibraryManagement.repository.LocacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {
    
    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private LocacaoRepository locacaoRepository;

    public List<Cliente> getAllCliente() {
        return clienteRepository.findAll();
    }

    public Cliente getClienteById(long id) {
    	Cliente cliente = clienteRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Cliente com id %d não encontrado.", id)
        	));

        return cliente;
    }

    public Cliente createCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente atualizaCliente(long id, Cliente newCliente) {
    	Cliente oldCliente = clienteRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Cliente com id %d não encontrado.", id)
        	));

        Cliente cliente = oldCliente;
        cliente.setCodigo(newCliente.getCodigo());
        cliente.setNome(newCliente.getNome());
        return clienteRepository.save(cliente);
    }

    public void deleteCliente(long id) {
    	Cliente cliente = clienteRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Cliente com id %d não encontrado.", id)
        	));

    	List<Locacao> locacaoList = locacaoRepository.getLocacaoByClienteId(cliente.getId());
    	if (locacaoList.size() > 0) {
    		locacaoList.forEach(locacaoRepository::delete);
        }
    	clienteRepository.delete(cliente);
    }
}
