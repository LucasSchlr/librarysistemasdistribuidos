package br.faccat.engsoft.LibraryManagement.service;

import br.faccat.engsoft.LibraryManagement.config.ApiException;
import br.faccat.engsoft.LibraryManagement.dto.LivroDto;
import br.faccat.engsoft.LibraryManagement.entity.Autor;
import br.faccat.engsoft.LibraryManagement.entity.Categoria;
import br.faccat.engsoft.LibraryManagement.entity.Cliente;
import br.faccat.engsoft.LibraryManagement.entity.Livro;
import br.faccat.engsoft.LibraryManagement.repository.AutorRepository;
import br.faccat.engsoft.LibraryManagement.repository.CategoriaRepository;
import br.faccat.engsoft.LibraryManagement.repository.ClienteRepository;
import br.faccat.engsoft.LibraryManagement.repository.LivroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LivroService {

    @Autowired
    private LivroRepository livroRepository;

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Autowired
    private AutorRepository autorRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public List<Livro> getAllLivros() {
        return livroRepository.findAll();
    }

    public Optional<Livro> getLivroById(Long id) {
        return livroRepository.findById(id);
    }

    public Livro createLivro(LivroDto livroDto) {
        Livro livro = new Livro();
        livro.setTitulo(livroDto.getTitulo());
        livro.setPreco(livroDto.getPreco());
        livro.setQuantidadeDisponivel(livroDto.getQuantidadeDisponivel());
        livro.setCategoria(categoriaRepository.getById(livroDto.getCategoriaId()));
        livro.setAutor(autorRepository.getById(livroDto.getAutorId()));
        return livroRepository.save(livro);
    }

    public List<Livro> getByNomeCliente(String nomeCliente) {
        Cliente cliente = clienteRepository.getClienteByName(nomeCliente);
        return livroRepository.getLivrosByClienteIdAndAtivoTrue(cliente.getId());
    }

    public List<Livro> getLivrosByNomeAutor(String nomeAutor) {
        Autor autor = autorRepository.getAutorByName(nomeAutor);
        return livroRepository.getLivrosByAutorId(autor.getId());
    }

    public List<Livro> getLivrosByCategoria(String nomeCategoria) {
        Categoria categoria = categoriaRepository.getCategoriaByName(nomeCategoria);
        return livroRepository.getLivrosByCategoriaId(categoria.getId());
    }

    public Livro atualizaLivro(long id, LivroDto newLivroDto) {
    	Livro oldLivro = livroRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Livro com id %d não encontrado.", id)
        	));
    	
    	Livro livro = oldLivro;
    	livro.setTitulo(newLivroDto.getTitulo());
    	livro.setPreco(newLivroDto.getPreco());
    	livro.setQuantidadeDisponivel(newLivroDto.getQuantidadeDisponivel());
    	livro.setCategoria( categoriaRepository.getById(newLivroDto.getCategoriaId()) );
    	livro.setAutor( autorRepository.getById(newLivroDto.getAutorId()) );

    	return livroRepository.save(livro);
    }

    public void deleteLivro(long id) {
    	Livro livro = livroRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Livro com id %d não encontrado.", id)
        	));
    	
    	livroRepository.delete(livro);
    }
}
