package br.faccat.engsoft.LibraryManagement.service;

import br.faccat.engsoft.LibraryManagement.config.ApiException;
import br.faccat.engsoft.LibraryManagement.dto.LivroClienteDto;
import br.faccat.engsoft.LibraryManagement.entity.Cliente;
import br.faccat.engsoft.LibraryManagement.entity.Livro;
import br.faccat.engsoft.LibraryManagement.entity.Locacao;
import br.faccat.engsoft.LibraryManagement.repository.ClienteRepository;
import br.faccat.engsoft.LibraryManagement.repository.LivroRepository;
import br.faccat.engsoft.LibraryManagement.repository.LocacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LocacaoService {

    @Autowired
    private LocacaoRepository locacaoRepository;

    @Autowired
    private LivroRepository livroRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public List<Locacao> getAllLocacao() {
    	List<Locacao> locacoes = new ArrayList<Locacao>();
    	
    	for (Locacao locacao: locacaoRepository.findAll()) {
    		if (locacao.getAtivo()) {
    			locacoes.add(locacao);
    		}
    	}
    	
        return locacoes;
    }

    public Locacao getLocacaoById(long id) {
        Locacao locacao = locacaoRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Locação com id %d não econtrada.", id)
        	));

        return locacao;        
    }

    public Locacao createLocacao(LivroClienteDto livroCliente) {
        Livro livro = livroRepository.findById(livroCliente.getLivroId()).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Livro com id %d não encontrado.", livroCliente.getLivroId())
        	));
        Cliente cliente = clienteRepository.findById(livroCliente.getClienteId()).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND,String.format("Cliente com id %d não encontrado.", livroCliente.getClienteId())
        	));

        if (livro.getQuantidadeDisponivel() > 0) {
        	Locacao locacao = new Locacao();
        	locacao.setAtivo(true);
        	locacao.setCliente(cliente);
        	locacao.setLivro(livro);
        	locacaoRepository.save(locacao);
        	
        	livro.setQuantidadeDisponivel(livro.getQuantidadeDisponivel() - 1);
        	livroRepository.save(livro);
        	return locacao;
        } else {
        	throw new ApiException(HttpStatus.BAD_REQUEST, "Livro indisponível.");
        }
    }

    public Locacao devolveLivro (long id) {
        Locacao oldLocacao = locacaoRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Locação com id %d não econtrada.", id)
        	));
        
        Locacao locacao = oldLocacao;
        locacao.setAtivo(false);
        locacaoRepository.save(locacao);
        Livro livro = livroRepository.getById(locacao.getLivro().getId());
        livro.setQuantidadeDisponivel(livro.getQuantidadeDisponivel() + 1);
        livroRepository.save(livro);
        return locacao;
    }

    public void deleteLocacao(long id) {
        Locacao locacao = locacaoRepository.findById(id).orElseThrow(
        		() -> new ApiException(HttpStatus.NOT_FOUND, String.format("Locação com id %d não econtrada.", id)
        	));

        locacaoRepository.delete(locacao);
    }

}
