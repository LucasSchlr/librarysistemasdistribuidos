package br.faccat.engsoft.LibraryManagement.service;

import br.faccat.engsoft.LibraryManagement.config.ApiException;
import br.faccat.engsoft.LibraryManagement.dto.LoginDto;
import br.faccat.engsoft.LibraryManagement.entity.Usuario;
import br.faccat.engsoft.LibraryManagement.repository.UsuarioRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<Usuario> getAllUsuario() {
        return usuarioRepository.findAll();
    }

    public Usuario getUsuarioById(long id) {
        Optional<Usuario> usuario = usuarioRepository.findById(id);
        if(usuario.isPresent()) return usuario.get();
        throw new ApiException(HttpStatus.NOT_FOUND, String.format("Usuario com id %d não encontrado.", id));
    }

    public Boolean login(LoginDto login) {
        Usuario usuario = usuarioRepository.getByUsername(login.getUsuario());
        if (verificaSenhaHash(login.getSenha(), usuario.getSenha())) return true;
        return false;
    }

    public Usuario createUsuario(Usuario usuario) {
        usuario.setSenha(criaSenhaHash(usuario.getSenha()));
        return usuarioRepository.save(usuario);
    }

    public Usuario atualizaUsuario(long id, Usuario newUsuario) {
        Optional<Usuario> oldUsuario = usuarioRepository.findById(id);
        if(oldUsuario.isPresent()){
            Usuario usuario = oldUsuario.get();
            usuario.setLogin(newUsuario.getLogin());
            usuario.setSenha(criaSenhaHash(newUsuario.getSenha()));
            usuario.setNome(newUsuario.getNome());
            return usuarioRepository.save(usuario);
        }
        else
            throw new ApiException(HttpStatus.NOT_FOUND, String.format("Usuario com id %d não encontrado.", id));
    }

    public void deleteUsuario(long id) {
        Optional<Usuario> usuario = usuarioRepository.findById(id);
        if(usuario.isPresent()){
            usuarioRepository.delete(usuario.get());
        }
        else
            throw new ApiException(HttpStatus.NOT_FOUND, String.format("Usuario com id %d não encontrado.", id));
    }

    private String criaSenhaHash(String senha) {
        return BCrypt.hashpw(senha, BCrypt.gensalt());
    }

    private Boolean verificaSenhaHash(String senha, String hash) {
        return BCrypt.checkpw(senha, hash);
    }

}
