import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form'
import {Container, Row } from 'react-bootstrap';
import { Autor, emptyAutor } from '../../interfaces/autor/Autor'
import {salvarAutor, autorPorId, alterarAutor, deletaAutor} from '../../services/AutorService'
import { useRouter } from 'next/router'
import Layout from '../utils/Layout'
import {ControlButtonsContainer, BotaoCancelar, BotaoSalvar, IDTextContainer }  from '../../styles/styled-components/utils/FormStyles'

type Props = {
    modoInsert: boolean,
    idAutor:number
}
const AutorForm = ({modoInsert, idAutor}:Props) => {
    const [autor, setAutor] = useState<Autor>(emptyAutor())
    const router = useRouter()

    async function consultaDados() {
        if (idAutor > 0){
            autorPorId(idAutor).then((dados) => {
                if (dados){
                    setAutor(dados)
                }
            })
        }
    }

    async function handleSaveButton(){
        if (modoInsert){
            salvarAutor({...autor}).then((dados) => {
                router.push('../autor/cadastro/'+dados.id)
            })
        }else{
            alterarAutor(autor)
        }
    }

    async function handleDeleteButton(){
        deletaAutor(idAutor)
    }

    useEffect(() => {
        consultaDados()
    }, [idAutor])

    async function handleCancelarButton(){
        
        router.push('/autor/')
    }

    return (
        <Layout>
            <Container fluid="sm">
                <Row className="justify-content-md-center">
                    <Form>
                        <Form.Group >
                            <Form.Label>Autor</Form.Label>
                            <IDTextContainer>
                                <Form.Control
                                    size="sm"
                                    type="lightText"
                                    value={autor.id}
                                    readOnly={true}
                                />
                                {(!modoInsert) ? 
                                    <ControlButtonsContainer>

                                        <BotaoCancelar onClick={handleDeleteButton}>
                                            Deletar
                                        </BotaoCancelar>
                                    </ControlButtonsContainer>:null}                                
                            </IDTextContainer>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Nome</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={autor.nome}
                                onChange={e => setAutor({...autor, nome:e.target.value } )}
                            />
                        </Form.Group>
                        <ControlButtonsContainer>
                            <BotaoSalvar onClick={handleSaveButton}>
                                Salvar
                            </BotaoSalvar>
                            <BotaoCancelar onClick={handleCancelarButton}>
                                Cancelar
                            </BotaoCancelar>
                        </ControlButtonsContainer>
                    </Form>
                </Row>
            </Container >
        </Layout>)

}
export default AutorForm;

