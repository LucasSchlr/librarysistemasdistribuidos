import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form'
import {Container, Row } from 'react-bootstrap';
import { Categoria, emptyCategoria } from '../../interfaces/categoria/Categoria'
import {salvarCategoria, categoriaPorId, alterarCategoria, deletaCategoria} from '../../services/CategoriaService'
import { useRouter } from 'next/router'
import Layout from '../utils/Layout'
import {ControlButtonsContainer, BotaoCancelar, BotaoSalvar, IDTextContainer }  from '../../styles/styled-components/utils/FormStyles'

type Props = {
    modoInsert: boolean,
    idCategoria:number
}
const CategoriaForm = ({modoInsert, idCategoria}:Props) => {
    const [categoria, setCategoria] = useState<Categoria>(emptyCategoria())
    const router = useRouter()

    async function consultaDados() {
        if (idCategoria > 0){
            categoriaPorId(idCategoria).then((dados) => {
                if (dados){
                    setCategoria(dados)
                }
            })
        }
    }

    async function handleSaveButton(){
        if (modoInsert){
            salvarCategoria({...categoria}).then((dados) => {
                router.push('../categoria/cadastro/'+dados.id)
            })
        }else{
            alterarCategoria(categoria)
        }
    }

    async function handleDeleteButton(){
        deletaCategoria(idCategoria)
    }

    useEffect(() => {
        consultaDados()
    }, [idCategoria])

    async function handleCancelarButton(){
        
        router.push('/categoria/')
    }

    return (
        <Layout>
            <Container fluid="sm">
                <Row className="justify-content-md-center">
                    <Form>
                        <Form.Group >
                            <Form.Label>Marca</Form.Label>
                            <IDTextContainer>
                                <Form.Control
                                    size="sm"
                                    type="lightText"
                                    value={categoria.id}
                                    readOnly={true}
                                />
                                {(!modoInsert) ? 
                                    <ControlButtonsContainer>

                                        <BotaoCancelar onClick={handleDeleteButton}>
                                            Deletar
                                        </BotaoCancelar>
                                    </ControlButtonsContainer>:null}                                
                            </IDTextContainer>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Descrição</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={categoria.descricao}
                                onChange={e => setCategoria({...categoria, descricao:e.target.value } )}
                            />
                        </Form.Group>
                        <ControlButtonsContainer>
                            <BotaoSalvar onClick={handleSaveButton}>
                                Salvar
                            </BotaoSalvar>
                            <BotaoCancelar onClick={handleCancelarButton}>
                                Cancelar
                            </BotaoCancelar>
                        </ControlButtonsContainer>
                    </Form>
                </Row>
            </Container >
        </Layout>)

}
export default CategoriaForm;

