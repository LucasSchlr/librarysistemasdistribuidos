import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form'
import {Container, Row } from 'react-bootstrap';
import { Cliente, emptyCliente } from '../../interfaces/cliente/Cliente'
import {salvarCliente, clientePorId, alterarCliente, deletaCliente} from '../../services/ClienteService'
import { useRouter } from 'next/router'
import Layout from '../utils/Layout'
import {ControlButtonsContainer, BotaoCancelar, BotaoSalvar, IDTextContainer }  from '../../styles/styled-components/utils/FormStyles'

type Props = {
    modoInsert: boolean,
    idCliente:number
}
const ClienteForm = ({modoInsert, idCliente}:Props) => {
    const [Cliente, setCliente] = useState<Cliente>(emptyCliente())
    const router = useRouter()

    async function consultaDados() {
        if (idCliente > 0){
            clientePorId(idCliente).then((dados) => {
                if (dados){
                    setCliente(dados)
                }
            })
        }
    }

    async function handleSaveButton(){
        if (modoInsert){
            salvarCliente({...Cliente}).then((dados) => {
                router.push('../cliente/cadastro/'+dados.id)
            })
        }else{
            alterarCliente(Cliente)
        }
    }

    async function handleDeleteButton(){
        deletaCliente(idCliente)
    }

    useEffect(() => {
        consultaDados()
    }, [idCliente])

    async function handleCancelarButton(){
        
        router.push('/cliente/')
    }

    return (
        <Layout>
            <Container fluid="sm">
                <Row className="justify-content-md-center">
                    <Form>
                        <Form.Group >
                            <Form.Label>Cliente</Form.Label>
                            <IDTextContainer>
                                <Form.Control
                                    size="sm"
                                    type="lightText"
                                    value={Cliente.id}
                                    readOnly={true}
                                />
                                {(!modoInsert) ? 
                                    <ControlButtonsContainer>

                                        <BotaoCancelar onClick={handleDeleteButton}>
                                            Deletar
                                        </BotaoCancelar>
                                    </ControlButtonsContainer>:null}                                
                            </IDTextContainer>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Código</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={Cliente.codigo}
                                onChange={e => setCliente({...Cliente, codigo:e.target.value } )}
                            />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Nome</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={Cliente.nome}
                                onChange={e => setCliente({...Cliente, nome:e.target.value } )}
                            />
                        </Form.Group>
                        <ControlButtonsContainer>
                            <BotaoSalvar onClick={handleSaveButton}>
                                Salvar
                            </BotaoSalvar>
                            <BotaoCancelar onClick={handleCancelarButton}>
                                Cancelar
                            </BotaoCancelar>
                        </ControlButtonsContainer>
                    </Form>
                </Row>
            </Container >
        </Layout>)

}
export default ClienteForm;

