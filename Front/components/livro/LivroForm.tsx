import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form'
import {Container, Row } from 'react-bootstrap';
import { LivroDto, emptyLivroDto } from '../../interfaces/livro/Livro'
import {salvarLivro, livroPorId, alterarLivro, deletaLivro} from '../../services/LivroService'
import { useRouter } from 'next/router'
import Layout from '../utils/Layout'
import {ControlButtonsContainer, BotaoCancelar, BotaoSalvar, IDTextContainer }  from '../../styles/styled-components/utils/FormStyles'

type Props = {
    modoInsert: boolean,
    idLivro:number
}
const LivroDtoForm = ({modoInsert, idLivro}:Props) => {
    const [LivroDto, setLivroDto] = useState<LivroDto>(emptyLivroDto())
    const router = useRouter()

    async function consultaDados() {
        if (idLivro > 0){
            livroPorId(idLivro).then((dados) => {
                if (dados){
                    setLivroDto(
                        {
                            ...dados,
                            autorId: Number(dados.autor.id),
                            categoriaId: Number(dados.categoria.id)
                        })
                }
            })
        }
    }

    async function handleSaveButton(){
        if (modoInsert){
            salvarLivro({...LivroDto}).then((dados) => {
                router.push('../livro/cadastro/'+dados.id)
            })
        }else{
            alterarLivro(LivroDto)
        }
    }

    async function handleDeleteButton(){
        deletaLivro(idLivro)
    }

    useEffect(() => {
        consultaDados()
    }, [idLivro])

    async function handleCancelarButton(){        
        router.push('/livro/')
    }

    return (
        <Layout>
            <Container fluid="sm">
                <Row className="justify-content-md-center">
                    <Form>
                        <Form.Group >
                            <Form.Label>Livro</Form.Label>
                            <IDTextContainer>
                                <Form.Control
                                    size="sm"
                                    type="lightText"
                                    value={LivroDto.id}
                                    readOnly={true}
                                />
                                {(!modoInsert) ? 
                                    <ControlButtonsContainer>

                                        <BotaoCancelar onClick={handleDeleteButton}>
                                            Deletar
                                        </BotaoCancelar>
                                    </ControlButtonsContainer>:null}                                
                            </IDTextContainer>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Titulo</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={LivroDto.titulo}
                                onChange={e => setLivroDto({...LivroDto, titulo:e.target.value } )}
                            />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Preço</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={LivroDto.preco}
                                onChange={e => setLivroDto({...LivroDto, preco:Number(e.target.value) } )}
                            />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Quantidade</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={LivroDto.quantidadeDisponivel}
                                onChange={e => setLivroDto({...LivroDto, quantidadeDisponivel:Number(e.target.value) } )}
                            />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Autor</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={LivroDto.autorId}
                                onChange={e => setLivroDto({...LivroDto, autorId:Number(e.target.value) } )}
                            />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Categoria</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={LivroDto.categoriaId}
                                onChange={e => setLivroDto({...LivroDto, categoriaId:Number(e.target.value) } )}
                            />
                        </Form.Group>
                        <ControlButtonsContainer>
                            <BotaoSalvar onClick={handleSaveButton}>
                                Salvar
                            </BotaoSalvar>
                            <BotaoCancelar onClick={handleCancelarButton}>
                                Cancelar
                            </BotaoCancelar>
                        </ControlButtonsContainer>
                    </Form>
                </Row>
            </Container >
        </Layout>)

}
export default LivroDtoForm;

