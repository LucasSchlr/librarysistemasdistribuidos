import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form'
import {Container, Row } from 'react-bootstrap';
import { LocacaoDto, emptyLocacao } from '../../interfaces/locacao/Locacao'
import {salvarLocacao, locacaoPorId, alterarLocacao, deletaLocacao} from '../../services/LocacaoService'
import { useRouter } from 'next/router'
import Layout from '../utils/Layout'
import {ControlButtonsContainer, BotaoCancelar, BotaoSalvar, IDTextContainer }  from '../../styles/styled-components/utils/FormStyles'

type Props = {
    modoInsert: boolean,
    idLocacao:number
}
const LocacaoForm = ({modoInsert, idLocacao}:Props) => {
    const [locacao, setLocacao] = useState<LocacaoDto>(emptyLocacao())
    const router = useRouter()

    async function consultaDados() {
        if (idLocacao > 0){
            locacaoPorId(idLocacao).then((dados) => {
                if (dados){
                    setLocacao(
                        {                            
                            id:dados.id,
                            livroId:Number(dados.livro.id),
                            clienteId:Number(dados.cliente.id),
                            ativo:dados.ativo
                        })
                }
            })
        }
    }

    async function handleSaveButton(){
        if (modoInsert){
            salvarLocacao({...locacao}).then((dados) => {
                router.push('../locacao/cadastro/'+dados.id)
            })
        }
    }

    async function handleDeleteButton(){
        deletaLocacao(idLocacao)
    }

    useEffect(() => {
        consultaDados()
    }, [idLocacao])

    async function handleCancelarButton(){
        router.push('/locacao/')
    }

    return (
        <Layout>
            <Container fluid="sm">
                <Row className="justify-content-md-center">
                    <Form>
                        <Form.Group >
                            <Form.Label>Locação</Form.Label>
                            <IDTextContainer>
                                <Form.Control
                                    size="sm"
                                    type="lightText"
                                    value={locacao.id}
                                    readOnly={true}
                                />
                                {(!modoInsert) ? 
                                    <ControlButtonsContainer>

                                        <BotaoCancelar onClick={handleDeleteButton}>
                                            Deletar
                                        </BotaoCancelar>
                                    </ControlButtonsContainer>:null}                                
                            </IDTextContainer>
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Cliente</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={locacao.clienteId}
                                onChange={e => setLocacao({...locacao, clienteId:Number(e.target.value) } )}
                            />
                        </Form.Group>
                        
                        <Form.Group >
                            <Form.Label>Livro</Form.Label>
                            <Form.Control
                                size="sm"
                                type="lightText"
                                value={locacao.livroId}
                                onChange={e => setLocacao({...locacao, livroId:Number(e.target.value) } )}
                            />
                        </Form.Group>
                        <ControlButtonsContainer>
                            <BotaoSalvar onClick={handleSaveButton}>
                                Salvar
                            </BotaoSalvar>
                            <BotaoCancelar onClick={handleCancelarButton}>
                                Cancelar
                            </BotaoCancelar>
                        </ControlButtonsContainer>
                    </Form>
                </Row>
            </Container >
        </Layout>)

}
export default LocacaoForm;