import { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import tableStyles from '../../styles/styled-components/marca/lista.module.css'
import { TableContainer, ScreenContentContainer } from '../../styles/styled-components/marca/ListagemFormStyles'
import { ControlButtonsContainer, ActionButtonsContainerLista, EditPesquisaContainer } from '../../styles/styled-components/utils/ListagemFormStyles'
import { Form } from 'react-bootstrap'
import { BotaoSalvar }  from '../../styles/styled-components/utils/FormStyles'
import { ListagemEntitade, DicionarioTables, RegistroDicionario } from '../../interfaces/pesquisa/ListagemEntidade'
import {Property} from 'csstype'
import Image from 'next/image'
import React from 'next'
import ModalConfirma from '../utils/ModalConfirma'
import compare from '../../public/utils/customSorting'

import downIcon from '../../public/images/down.png'
import upIcon from '../../public/images/up.png'
import editarIcon from '../../public/images/editar.png'
import deleteIcon from '../../public/images/delete_redX.png'

type Props<T> = {
    dicionario: DicionarioTables
    refreshData: () => Promise<ListagemEntitade<T>>
    handleEditButton?: (id: string) => {}
    deleteRegister?: (id: string) => {}
    handleNewButton?:() => {}
    botoesAuxiliares?:() => {}
}

const ListagemEntidadeForm  = <T extends unknown>({dicionario, deleteRegister, refreshData, handleEditButton, handleNewButton, botoesAuxiliares}: Props<T>) => {
 
    const [temDeleteRegister, setTemDeleteRegister] = useState<boolean>(false) 
    const [temHandleEditButton, setTemHandleEditButton] = useState<boolean>(false) 
    const [temHandleNewButton, setTemHandleNewButton] = useState<boolean>(false) 

    const [dicionarioTela, setDicionarioTela] = useState<DicionarioTables>(dicionario) 

    const [filtro, setFiltro] = useState<string>('') 
    const [registros, setRegistros] = useState<Array<T>>(Array<T>())        
    const [showConfirma, setShowConfirma] = useState<boolean>(false)
    const [idToDelete, setIdToDelete] = useState<string>("")
    const atualizaShow = () => {setShowConfirma(!showConfirma)}

    const handleDeleteButton = async (id:string) => {
        setIdToDelete(id)
        atualizaShow()
    }

    const newButtonClick = () => {
        if (handleNewButton) {
            handleNewButton()
        }
    }

    const editButtonClick = (id: string) => {
        if (handleEditButton) {
            handleEditButton(id)
        }
    }

    useEffect(() => {    
        if (deleteRegister){setTemDeleteRegister(true)}
        if (handleEditButton){setTemHandleEditButton(true)}
        if (handleNewButton){setTemHandleNewButton(true)}
    },[deleteRegister, handleEditButton, handleNewButton])


    useEffect(() => {
        refreshData().then((retorno) => {            
            setRegistros(retorno.dados)
        })
    }, [] )

    function handleKeyUp(event: any) {
        const key = event.keyCode
        if ([13, 9].includes(key)) {
            refreshData().then((retorno) => {
                setRegistros(retorno.dados)
            })
        }
    }

    const efetuaDelete = async () => {
        if (deleteRegister){
            await deleteRegister(idToDelete)
            refreshData().then((retorno) => {
                setRegistros(retorno.dados)
            })
        }
    }

    const handleTitleClick = (indexDicionario:number) => {
        const {jsonField, reverso} = dicionarioTela.registros[indexDicionario]
        const dicionarioTmp = {...dicionarioTela}
        dicionarioTmp.registros = dicionarioTmp.registros.map((registro, index) => {
                if (index == indexDicionario) return {...registro, reverso: !reverso, ordenador: true}
                return {...registro, ordenador: false, reverso:false }
            })     
        const listaTmp = [...registros.sort( (a, b) => compare(a, b, jsonField, reverso))]
        setDicionarioTela({...dicionarioTmp})
        setRegistros(listaTmp)
    }

    const returnOrderIcon = (registroDicionario:RegistroDicionario) => {
        if (registroDicionario.ordenador){
            if (registroDicionario.reverso){
                return <Image src={downIcon} />
            }
            return <Image src={upIcon}/>
        }
        return null
    }

    return (
        <ScreenContentContainer>
            <TableContainer>
                <ControlButtonsContainer>
                        <EditPesquisaContainer/>
                        {(temHandleNewButton) ? <BotaoSalvar onClick={newButtonClick}>Novo</BotaoSalvar> : null}
                    </ControlButtonsContainer>
                <div>
                    <table className={tableStyles.table}>
                        <thead>
                            <tr>
                                {dicionarioTela.registros.map((dicionario, index) => {
                                    const bold: Property.FontWeight = "bold"                                  
                                    const tableTitleCell = {
                                        fontWeight: bold,
                                        fontSize:'1.2em',
                                        padding:'2px 6px 2px 6px', 
                                        width:`${dicionario.proporcaoLargura}%`
                                    }
                                    return <td key={dicionario.titulo} style={tableTitleCell} onClick={() => {handleTitleClick(index)}}>{dicionario.titulo}  {returnOrderIcon(dicionario)}</td>
                                })}
                                <td />
                            </tr>
                        </thead>
                        <tbody>
                            {registros.map((registro: any, index: number) => {
                                return (
                                    <tr key={index} className={tableStyles.tableRow}>
                                        {dicionarioTela.registros.map((item) => {
                                            const tableCell = {
                                                padding: '2px 6px 2px 6px',
                                                width:`${item.proporcaoLargura}%`
                                            }
                                            return <td key={index + item.jsonField} style={tableCell}>{registro[item.jsonField]}</td>
                                        })}
                                        <td>
                                            <ActionButtonsContainerLista>
                                                {(temHandleEditButton) ? <ActionButtonsContainerLista><Image onClick={e => editButtonClick(registro[dicionario.idJsonField])} src={editarIcon} /></ActionButtonsContainerLista>: null}
                                                {(temDeleteRegister) ? <ActionButtonsContainerLista><Image onClick={e => {                                                                                                        
                                                    console.log(registro, dicionario.idJsonField)
                                                    handleDeleteButton(registro[dicionario.idJsonField]) }} src={deleteIcon} /> </ActionButtonsContainerLista>: null}
                                            </ActionButtonsContainerLista >
                                        </td>
                                    </tr>)
                            })}
                        </tbody>
                    </table>
                </div>
            </TableContainer>
            <ModalConfirma
                config={{
                    textoPergunta:'Confirma a exclusão?',
                    showForm:showConfirma,
                    onConfirmou:efetuaDelete,
                    onClose:atualizaShow
                }}
            />
        </ScreenContentContainer>)
}

export default ListagemEntidadeForm