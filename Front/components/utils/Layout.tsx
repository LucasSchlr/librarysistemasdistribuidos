import React from 'react'
import Head from 'next/head'
import Image from 'next/image'
import utilStyles from '../../styles/utils.module.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { MainContent, MainContentContainer, MainContentCardContainer, HeaderContainer, ScreenTitleContainer, PageContainer} from '../../styles/styled-components/utils/main'

const name = 'Next.js'
export const siteTitle = 'Next.js Sample Website'

type Props = {
  screenTitle?: string,
  children: JSX.Element,
};

const Layout = ({ children, screenTitle }: Props) => {
  return (
    <PageContainer className='mainLayoutDiv'>
      <header>
        <HeaderContainer>
          <Navbar variant="dark" font-weigh="bold" >
            <Nav.Link href="/">
              <Navbar.Brand >
              </Navbar.Brand>
            </Nav.Link>
            <Nav.Link href="/autor"><label className={utilStyles.cadastroLink}>Autor</label></Nav.Link>
            <Nav.Link href="/categoria"><label className={utilStyles.cadastroLink}>Categoria</label></Nav.Link>
            <Nav.Link href="/cliente"><label className={utilStyles.cadastroLink}>Cliente</label></Nav.Link>
            <Nav.Link href="/livro"><label className={utilStyles.cadastroLink}>Livro</label></Nav.Link>
            <Nav.Link href="/locacao"><label className={utilStyles.cadastroLink}>Locação</label></Nav.Link>
          </Navbar>
        </HeaderContainer>
      </header>
      <MainContent>
        <MainContentContainer>
          {(screenTitle) ? <ScreenTitleContainer>{screenTitle}</ScreenTitleContainer> : null}
          <MainContentCardContainer>
            {children}
          </MainContentCardContainer>
        </MainContentContainer>
      </MainContent>
    </PageContainer>
  )
}

export default Layout