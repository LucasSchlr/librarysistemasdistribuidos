import { ErroAPI } from '../../interfaces/api/ErroAPI'
import moment from "moment"
import modalErroStyles from '../../styles/styled-components/utils/erroModal.module.css'


function criaHeader(ModalOverlay:HTMLDivElement, Modal:HTMLDivElement ){
    const Header = document.createElement('div')
    Header.className = modalErroStyles.styleHeader

    const Erro = document.createElement('div')
    Erro.innerHTML = '<h3>Erro</h3>'
    Header.appendChild(Erro)

    const Close = document.createElement('div')
    Close.className = modalErroStyles.styleClose
    Close.innerHTML = 'X'
    Close.onclick = () => {
        ModalOverlay.remove()
        Modal.remove()
    }
    Header.appendChild(Close)
    
    return Header
}

function criaBody(objetoErro:ErroAPI){
    const Body = document.createElement('div')
    Body.className = modalErroStyles.styleBody
    Body.innerHTML = `<p>Código: <b>${objetoErro.status}</b><br />
                     Mensagem: <b>${objetoErro.erro}</b></p>
                     <p><b>${moment().format('DD/MM/yyyy HH:m:ss')}</b></p>`
    return Body
}

function criaBottom(){    
    const Bottom = document.createElement('div')
    Bottom.className = modalErroStyles.styleBottom

    return Bottom
}

function modalErro(objetoErro:ErroAPI){

    const modalAnterior = document.querySelector(`div[key=modal]`) as HTMLDivElement
    if (modalAnterior){
        modalAnterior.remove()
    }

    const ModalOverlay = document.createElement('div')
    ModalOverlay.setAttribute('key', 'modal') 
    ModalOverlay.className = modalErroStyles.styleModalOverlay

    const Modal = document.createElement('div')
    Modal.className = modalErroStyles.styleModal
    
    const ModalContent = document.createElement('div')
    ModalContent.className = modalErroStyles.styleModalContent

    ModalContent.appendChild(criaHeader(ModalOverlay, Modal))
    ModalContent.appendChild(criaBody(objetoErro))
    ModalContent.appendChild(criaBottom())
    Modal.appendChild(ModalContent)
    ModalOverlay.appendChild(Modal)

    return ModalOverlay
}

export default function adicionaModalErro(objetoErro:ErroAPI){
    //const nextSibling = document.querySelector(`div[key=${globalConstants.layoutPageGlobalName}]`) as HTMLDivElement
    const nextSibling = document.querySelector('.mainLayoutDiv') as HTMLDivElement    
    nextSibling.appendChild(modalErro(objetoErro))    
}