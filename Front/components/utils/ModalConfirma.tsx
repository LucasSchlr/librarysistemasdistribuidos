import React, { useState, useEffect } from 'react'
import { Modal } from "react-bootstrap"
import { BotaoSalvar, BotaoCancelar }  from '../../styles/styled-components/utils/FormStyles'

export interface ConfiguraConfirma{
    textoPergunta:string,
    // idConfirmacao?:string,
    showForm:boolean,
    onConfirmou?: () => void,
    onClose:() => void
} 

type Props = {
    config:ConfiguraConfirma
}

const ModalConfirma = ({config}:Props) => {    
    const [show, setShow] = useState<boolean>(config.showForm) 
    const [textoPerguntaTela, setTextoPerguntaTela] = useState<string>(config.textoPergunta)
    // const [idConfirmacaoTela, setIdConfirmacaoTela] = useState<string|undefined>(config.idConfirmacao)

    useEffect(() => {
        setShow(config.showForm)
    }, [config.showForm])

    useEffect(() => {
        setTextoPerguntaTela(config.textoPergunta)
    }, [config.textoPergunta])

    // useEffect(() => {
    //     setIdConfirmacaoTela(config.idConfirmacao)
    // }, [config.idConfirmacao])

    const handleClicouConfirmou = async () => {
        if (config.onConfirmou){
            await config.onConfirmou()
            config.onClose()
        }
    }

    return (
        <Modal
            show={show}
            backdrop="static"
            aria-labelledby="contained-modal-title-vcenter">
                <Modal.Header>
                    <Modal.Title>Atenção</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {textoPerguntaTela}
                </Modal.Body>
                <Modal.Footer>                
                    <BotaoSalvar onClick={handleClicouConfirmou}>
                        Confirmar
                    </BotaoSalvar>           
                    <BotaoCancelar onClick={() => {config.onClose()}}>
                        Cancelar
                    </BotaoCancelar>
                </Modal.Footer>
        </Modal>
    )
}
export default ModalConfirma
