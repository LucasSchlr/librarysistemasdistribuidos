
export enum AmbienteEnum {
  HML = 'HML',
  PRD = 'PRD',
}
export interface AmbienteInterface {
  ambiente: AmbienteEnum;
  url: string;
}

const ambienteHomologacao: AmbienteInterface = {
  ambiente: AmbienteEnum.HML,
  url: 'http://localhost:8081/'
  //url: 'http://LB-LibraryManagement-1696586259.us-east-1.elb.amazonaws.com/'
};

export const getEnvironment = (): AmbienteInterface => {
  return ambienteHomologacao;
};
