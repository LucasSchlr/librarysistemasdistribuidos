import React, { useState, useEffect }  from 'react'
import { NextPage } from 'next'
import EditPesquisa from '../components/pesquisa/EditPesquisa'
import {Form, Container} from 'react-bootstrap'
import Layout from '../components/utils/Layout'
const IndexPage: NextPage = () => {

  return (
    <Layout>
      <Container fluid="sm">
        <Form/>
      </Container>
    </Layout>
  )

}
export default IndexPage
