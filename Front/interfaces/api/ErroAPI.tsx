export interface ErroAPI{
    status: number
    erro: string
    hora: string
    origem: string
    metodo: string
}

export function emptyErroAPI(){
    return {
        status: 0,
        erro: '',
        hora: '',
        origem: '',
        metodo: ''   
    }
}