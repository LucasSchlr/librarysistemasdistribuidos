export interface Autor{
    id:string
    nome:string
}

export function emptyAutor(): Autor{
    return {
        id:'',
        nome:''
    }
}