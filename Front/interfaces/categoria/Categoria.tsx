export interface Categoria{
    id:string
    descricao:string
}

export function emptyCategoria(): Categoria{
    return {
        id:'',
        descricao:''
    }
}