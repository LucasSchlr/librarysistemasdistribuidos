export interface Cliente{
    id:string
    nome:string
    codigo:string
}

export function emptyCliente(): Cliente{
    return {
        id:'',
        nome:'',
        codigo:''
    }
}