import {Autor} from "../autor/Autor"
import {Categoria} from "../categoria/Categoria"

export interface LivroDto{
    id:string
    titulo:string
    preco:number
    autorId:number
    categoriaId:number
    quantidadeDisponivel:number
}

export interface LivroConsulta{
    id:string
    titulo:string
    preco:number
    autor:string
    categoria:string
    quantidadeDisponivel:number
}


export interface Livro{
    id:string
    titulo:string
    preco:number
    autor:Autor
    categoria:Categoria
    quantidadeDisponivel:number
}

export function emptyLivroDto(): LivroDto{
    return {
        id:'',
        titulo:'',
        preco:0,
        autorId:0,
        categoriaId:0,
        quantidadeDisponivel:0
    }
}