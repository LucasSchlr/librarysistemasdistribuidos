import {Livro} from "../livro/Livro"
import {Cliente} from "../cliente/Cliente"

export interface LocacaoDto{
    id:string
    livroId:number
    clienteId:number
    ativo:Boolean
}

export interface LocacaoConsulta{
    id:string
    livroTitulo:string
    clienteNome:string
    ativo:Boolean
}


export interface Locacao{
    id:string
    livro:Livro
    cliente:Cliente
    ativo:Boolean
}

export function emptyLocacao(): LocacaoDto{
    return {
        id:'',
        livroId:0,
        clienteId:0,
        ativo:false
    }
}