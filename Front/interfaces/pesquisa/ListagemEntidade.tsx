export interface RegistroDicionario{
    titulo: string
    jsonField: string    
    proporcaoLargura:number
    ordenador:boolean
    reverso:boolean
}

export interface DicionarioTables{
    idJsonField: string
    registros: Array<RegistroDicionario>
}

export interface ListagemEntitade<T>{
    dados:Array<T>;
}