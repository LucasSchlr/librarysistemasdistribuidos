module.exports = {
  env: {
    API_HOST: 'http://localhost:8081',
  },
  images: {
    domains: ['www.firebasestorage.googleapis.com'],
  }
}
