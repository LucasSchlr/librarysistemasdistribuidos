import { NextApiRequest, NextApiResponse } from "next"
import { getEnvironment } from '../../../config/constant.config'
import axios from 'axios'

const env = getEnvironment()
const service = 'livro'

export default async (req: NextApiRequest, res: NextApiResponse) => {
    let response = null;
    try {
        response = await axios.delete(`${env.url}${service}/${req.query.id}`)
        res.status(200).json(response.data)
    }catch (err:any){
        console.log(err)
        res.status(err.response.status).json( err.response.data)
    }
}