import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import AutorForm from '../../../components/autor/AutorForm'
import { useRouter } from 'next/router'

const UpdateAutor: NextPage = () => {
    const router = useRouter()
    const idAutor  = Number(router.query.cadastroAutor)

    return <AutorForm
            idAutor={idAutor}
                modoInsert={false}/>
}

export default UpdateAutor