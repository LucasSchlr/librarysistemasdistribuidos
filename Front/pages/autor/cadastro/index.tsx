import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import AutorForm from '../../../components/autor/AutorForm'

const CadastroAutor: NextPage = () => {
    return  <AutorForm
    idAutor={0}
        modoInsert={true}
    />
}

export default CadastroAutor
