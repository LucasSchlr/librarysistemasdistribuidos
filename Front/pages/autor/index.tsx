import 'bootstrap/dist/css/bootstrap.min.css'
import { Autor } from '../../interfaces/autor/Autor'
import { deletaAutor, listaAutores } from '../../services/AutorService'
import { useRouter } from 'next/router'
import Layout from '../../components/utils/Layout'
import ListagemEntidadeForm from '../../components/pesquisa/ListagemEntidadeForm'
import { NextPage } from 'next'
import { ListagemEntitade } from '../../interfaces/pesquisa/ListagemEntidade'

const ListaMarcas: NextPage = () => {
    const router = useRouter()

    const handleDeleteButton = async (id:string) => {
        console.log('id', id)
        await deletaAutor(Number(id))
    }

    const handleEditButton = async (id:string) => {
        router.push('../autor/cadastro/'+id)
    }    

    const handleNewButton = async () => {        
        router.push('../autor/cadastro')
    }

    const refreshData = () => {
        return new Promise(async (resolve: (data:ListagemEntitade<Autor>) => void) => {
            const response = await listaAutores()
            resolve({dados:response})
        })
    }

    const dicionarioTables = {
        idJsonField: "id",
        descriptionJsonField:"nome",
        registros: [
            {titulo: "ID", jsonField: "id", proporcaoLargura: 15, ordenador:false, reverso:false},
            {titulo: "Nome", jsonField: "nome", proporcaoLargura: 85, ordenador:false, reverso:false}
        ] 
    }

    return  ( 
        <Layout
            screenTitle='Autores'>
            <ListagemEntidadeForm
                dicionario={dicionarioTables}
                refreshData={refreshData}
                handleEditButton={handleEditButton}
                deleteRegister={handleDeleteButton}
                handleNewButton={handleNewButton} 
            />
        </Layout>)
}

export default ListaMarcas