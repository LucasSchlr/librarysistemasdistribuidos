import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import CategoriaForm from '../../../components/categoria/CategoriaForm'
import { useRouter } from 'next/router'

const UpdateCategoria: NextPage = () => {
    const router = useRouter()
    const idCategoria = Number(router.query.cadastroCategoria)

    return <CategoriaForm
            idCategoria={idCategoria}
                modoInsert={false}/>
}

export default UpdateCategoria