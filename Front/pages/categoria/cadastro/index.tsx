import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import CategoriaForm from '../../../components/categoria/CategoriaForm'

const CadastroCategoria: NextPage = () => {
    return  <CategoriaForm
        idCategoria={0}
        modoInsert={true}
    />
}

export default CadastroCategoria