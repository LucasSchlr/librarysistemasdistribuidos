import 'bootstrap/dist/css/bootstrap.min.css'
import { Categoria } from '../../interfaces/categoria/Categoria'
import { deletaCategoria, listaCategorias } from '../../services/CategoriaService'
import { useRouter } from 'next/router'
import Layout from '../../components/utils/Layout'
import ListagemEntidadeForm from '../../components/pesquisa/ListagemEntidadeForm'
import { NextPage } from 'next'
import { ListagemEntitade } from '../../interfaces/pesquisa/ListagemEntidade'

const ListaMarcas: NextPage = () => {
    const router = useRouter()

    const handleDeleteButton = async (id:string) => {
        await deletaCategoria(Number(id))
    }

    const handleEditButton = async (id:string) => {
        router.push('../categoria/cadastro/'+id)
    }    

    const handleNewButton = async () => {        
        router.push('../categoria/cadastro')
    }

    const refreshData = () => {
        return new Promise(async (resolve: (data:ListagemEntitade<Categoria>) => void) => {
            const response = await listaCategorias()
            resolve({dados:response})
        })
    }

    const dicionarioTables = {
        idJsonField: "id",
        descriptionJsonField:"nome",
        registros: [
            {titulo: "ID", jsonField: "id", proporcaoLargura: 15, ordenador:false, reverso:false},
            {titulo: "Descrição", jsonField: "descricao", proporcaoLargura: 85, ordenador:false, reverso:false}
        ] 
    }

    return  ( 
        <Layout
            screenTitle='Categorias'>
            <ListagemEntidadeForm
                dicionario={dicionarioTables}
                refreshData={refreshData}
                handleEditButton={handleEditButton}
                deleteRegister={handleDeleteButton}
                handleNewButton={handleNewButton} 
            />
        </Layout>)
}

export default ListaMarcas