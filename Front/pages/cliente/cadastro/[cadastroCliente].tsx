import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import ClienteForm from '../../../components/cliente/ClienteForm'
import { useRouter } from 'next/router'

const UpdateCliente: NextPage = () => {
    const router = useRouter()
    const idCliente = Number(router.query.cadastroCliente)

    return <ClienteForm
        idCliente={idCliente}
        modoInsert={false}/>
}

export default UpdateCliente