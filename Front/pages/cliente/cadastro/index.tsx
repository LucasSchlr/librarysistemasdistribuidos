import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import ClienteForm from '../../../components/cliente/ClienteForm'

const CadastroCliente: NextPage = () => {
    return  <ClienteForm
        idCliente={0}
        modoInsert={true}
    />
}

export default CadastroCliente