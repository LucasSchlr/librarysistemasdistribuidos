import 'bootstrap/dist/css/bootstrap.min.css'
import { Cliente } from '../../interfaces/cliente/Cliente'
import { deletaCliente, listaClientes } from '../../services/ClienteService'
import { useRouter } from 'next/router'
import Layout from '../../components/utils/Layout'
import ListagemEntidadeForm from '../../components/pesquisa/ListagemEntidadeForm'
import { NextPage } from 'next'
import { ListagemEntitade } from '../../interfaces/pesquisa/ListagemEntidade'

const ListaClientes: NextPage = () => {
    const router = useRouter()

    const handleDeleteButton = async (id:string) => {
        await deletaCliente(Number(id))
    }

    const handleEditButton = async (id:string) => {
        router.push('../cliente/cadastro/'+id)
    }    

    const handleNewButton = async () => {        
        router.push('../cliente/cadastro')
    }

    const refreshData = () => {
        return new Promise(async (resolve: (data:ListagemEntitade<Cliente>) => void) => {
            const response = await listaClientes()
            resolve({dados:response})
        })
    }

    const dicionarioTables = {
        idJsonField: "id",
        descriptionJsonField:"nome",
        registros: [
            {titulo: "ID", jsonField: "id", proporcaoLargura: 15, ordenador:false, reverso:false},
            {titulo: "Nome", jsonField: "nome", proporcaoLargura: 85, ordenador:false, reverso:false}
        ] 
    }

    return  ( 
        <Layout
            screenTitle='Clientes'>
            <ListagemEntidadeForm
                dicionario={dicionarioTables}
                refreshData={refreshData}
                handleEditButton={handleEditButton}
                deleteRegister={handleDeleteButton}
                handleNewButton={handleNewButton} 
            />
        </Layout>)
}

export default ListaClientes