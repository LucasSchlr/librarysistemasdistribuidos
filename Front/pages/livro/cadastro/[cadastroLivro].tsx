import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import LivroForm from '../../../components/livro/LivroForm'
import { useRouter } from 'next/router'

const UpdateLivro: NextPage = () => {
    const router = useRouter()
    const idLivro = Number(router.query.cadastroLivro)

    return <LivroForm
        idLivro={idLivro}
        modoInsert={false}/>
}

export default UpdateLivro