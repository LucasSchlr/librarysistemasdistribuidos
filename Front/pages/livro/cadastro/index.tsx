import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import LivroForm from '../../../components/livro/LivroForm'

const CadastroLivro: NextPage = () => {
    return  <LivroForm
        idLivro={0}
        modoInsert={true}
    />
}

export default CadastroLivro