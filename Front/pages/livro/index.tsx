import 'bootstrap/dist/css/bootstrap.min.css'
import { LivroConsulta } from '../../interfaces/livro/Livro'
import { deletaLivro, listaLivros } from '../../services/LivroService'
import { useRouter } from 'next/router'
import Layout from '../../components/utils/Layout'
import ListagemEntidadeForm from '../../components/pesquisa/ListagemEntidadeForm'
import { NextPage } from 'next'
import { ListagemEntitade } from '../../interfaces/pesquisa/ListagemEntidade'

const ListaLivros: NextPage = () => {
    const router = useRouter()

    const handleDeleteButton = async (id:string) => {
        await deletaLivro(Number(id))
    }

    const handleEditButton = async (id:string) => {
        router.push('../livro/cadastro/'+id)
    }    

    const handleNewButton = async () => {        
        router.push('../livro/cadastro')
    }

    const refreshData = () => {
        return new Promise(async (resolve: (data:ListagemEntitade<LivroConsulta>) => void) => {
            const response = await listaLivros()

            const responseFormatado = response.map((item) => {
                return  {
                    id:item.id,
                    titulo:item.titulo,
                    preco:item.preco,
                    autor:item.autor.nome,
                    categoria:item.categoria.descricao,
                    quantidadeDisponivel:item.quantidadeDisponivel} })
            resolve({dados:responseFormatado})
        })
    }

    const dicionarioTables = {
        idJsonField: "id",
        descriptionJsonField:"nome",
        registros: [
            {titulo: "ID", jsonField: "id", proporcaoLargura: 15, ordenador:false, reverso:false},
            {titulo: "Título", jsonField: "titulo", proporcaoLargura: 35, ordenador:false, reverso:false},
            {titulo: "Autor", jsonField: "autor", proporcaoLargura: 25, ordenador:false, reverso:false},
            {titulo: "Categoria", jsonField: "categoria", proporcaoLargura: 25, ordenador:false, reverso:false}
        ] 
    }

    return  ( 
        <Layout
            screenTitle='Livros'>
            <ListagemEntidadeForm
                dicionario={dicionarioTables}
                refreshData={refreshData}
                handleEditButton={handleEditButton}
                deleteRegister={handleDeleteButton}
                handleNewButton={handleNewButton} 
            />
        </Layout>)
}

export default ListaLivros