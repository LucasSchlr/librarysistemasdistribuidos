import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import LocacaoForm from '../../../components/locacao/LocacaoForm'
import { useRouter } from 'next/router'

const UpdateLocacao: NextPage = () => {
    const router = useRouter()
    const idLocacao = Number(router.query.cadastroLocacao)

    return <LocacaoForm
        idLocacao={idLocacao}
        modoInsert={false}/>
}

export default UpdateLocacao