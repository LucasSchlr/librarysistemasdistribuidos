import 'bootstrap/dist/css/bootstrap.min.css'
import { NextPage } from 'next'
import LocacaoForm from '../../../components/locacao/LocacaoForm'

const CadastroLocacao: NextPage = () => {
    return  <LocacaoForm
        idLocacao={0}
        modoInsert={true}
    />
}

export default CadastroLocacao