import 'bootstrap/dist/css/bootstrap.min.css'
import { LocacaoConsulta } from '../../interfaces/locacao/Locacao'
import { alterarLocacao, listaLocacoes } from '../../services/LocacaoService'
import { useRouter } from 'next/router'
import Layout from '../../components/utils/Layout'
import ListagemEntidadeForm from '../../components/pesquisa/ListagemEntidadeForm'
import { NextPage } from 'next'
import { ListagemEntitade } from '../../interfaces/pesquisa/ListagemEntidade'

const ListaLocacoes: NextPage = () => {
    const router = useRouter()

    const handleDeleteButton = async (id:string) => {
        await alterarLocacao(Number(id))
    }

    const handleEditButton = async (id:string) => {
        router.push('../locacao/cadastro/'+id)
    }    

    const handleNewButton = async () => {        
        router.push('../locacao/cadastro')
    }

    const refreshData = () => {
        return new Promise(async (resolve: (data:ListagemEntitade<LocacaoConsulta>) => void) => {
            const response = await listaLocacoes()

            const responseFormatado = response.map((item) => {
                return  {
                    id:item.id,
                    livroTitulo:item.livro.titulo,
                    clienteNome:item.cliente.nome, 
                    ativo:item.ativo} })
            resolve({dados:responseFormatado})
        })
    }

    const dicionarioTables = {
        idJsonField: "id",
        descriptionJsonField:"nome",
        registros: [
            {titulo: "ID", jsonField: "id", proporcaoLargura: 15, ordenador:false, reverso:false},
            {titulo: "Título", jsonField: "livroTitulo", proporcaoLargura: 35, ordenador:false, reverso:false},
            {titulo: "Cliente", jsonField: "clienteNome", proporcaoLargura: 25, ordenador:false, reverso:false}
        ] 
    }

    return  ( 
        <Layout
            screenTitle='Locações ativas'>
            <ListagemEntidadeForm
                dicionario={dicionarioTables}
                refreshData={refreshData}
                handleEditButton={handleEditButton}
                deleteRegister={handleDeleteButton}
                handleNewButton={handleNewButton} 
            />
        </Layout>)
}

export default ListaLocacoes