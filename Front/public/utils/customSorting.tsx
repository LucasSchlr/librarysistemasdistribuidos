export default function compare(a: any, b: any, criteria: string, reverse: boolean){
    const order = (reverse ? -1 : 1);

  if ( a[criteria] < b[criteria] ){
    return -1 * order;
  }
  if ( a[criteria] > b[criteria] ){
    return 1* order;
  }
  return 0;
}