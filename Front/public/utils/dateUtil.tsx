function formataData(data:Date):string{
    const zeroPad = (num:number, places:number) => String(num).padStart(places, '0')
    return `${zeroPad(data.getDay(),2)}/${zeroPad(data.getMonth(),2)}/${data.getFullYear()}` 
}


export default formataData