function checkIfElementHasChildInputAndReturn(element: Element):Element|null{
    let childInput:Element|null = null
    element.childNodes.forEach((child: any) => {
        if (childInput){
            return 
        }
        if (child.nodeName.toLowerCase() === "input") {
            childInput = child
        }else{
            childInput = checkIfElementHasChildInputAndReturn(child)
        }
    })
    return childInput
}

function searchOnChild(current: Element):boolean{
    let next = current as Element
    let count = 0
    const maxCount = next?.parentElement?.childElementCount
    let found = false
    if (next && maxCount != null) {
        while (count <= maxCount){
            next = next.nextElementSibling as Element
            if (next == null){
                break
            }
            const inputOnNext = checkIfElementHasChildInputAndReturn(next) as HTMLInputElement
            if (inputOnNext) {
                found = true
                inputOnNext.focus();
                break
            }
            count ++
        }
    }
    return found

}

function moveToNextElement(current:Element){
    searchOnChild(current)
    // if (!searchOnChild(current)){        
    //     let next = current as Element
    //     const parent = next?.parentElement as Element
    //     if (parent){
    //         moveToNextElement(parent)
    //     }
    // }
}

export default moveToNextElement
