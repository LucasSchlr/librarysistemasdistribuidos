import axios from 'axios'
import { Autor } from '../interfaces/autor/Autor'
import adicionaModalErro from '../components/utils/LayoutErro'

export async function listaAutores():Promise<Array<Autor>> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/autor/getAutores')
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export async function autorPorId(id:number):Promise<Autor> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/autor/getAutoresPorId', {params:{id: id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export const alterarAutor = (autor:Autor) => {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.put('/api/autor/putAutor', autor, {params: {id:autor.id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const deletaAutor = (id:number) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.delete('api/autor/deleteAutor', {params: {id:id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const salvarAutor = (autor:Autor) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.post('/api/autor/postAutor', autor)
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}