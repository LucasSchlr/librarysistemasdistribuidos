import axios from 'axios'
import { Categoria } from '../interfaces/categoria/Categoria'
import adicionaModalErro from '../components/utils/LayoutErro'

export async function listaCategorias():Promise<Array<Categoria>> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/categoria/getCategoria')
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export async function categoriaPorId(id:number):Promise<Categoria> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/categoria/getCategoriaPorId', {params:{id: id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export const alterarCategoria = (categoria:Categoria) => {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.put('/api/categoria/putCategoria', categoria, {params: {id:categoria.id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const deletaCategoria = (id:number) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.delete('api/categoria/deleteCategoria', {params: {id:id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const salvarCategoria = (categoria:Categoria) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.post('/api/categoria/postCategoria', categoria)
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}