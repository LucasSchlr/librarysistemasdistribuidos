import axios from 'axios'
import { Cliente } from '../interfaces/cliente/Cliente'
import adicionaModalErro from '../components/utils/LayoutErro'

export async function listaClientes():Promise<Array<Cliente>> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/cliente/getCliente')
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export async function clientePorId(id:number):Promise<Cliente> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/cliente/getClientePorId', {params:{id: id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export const alterarCliente = (cliente:Cliente) => {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.put('/api/cliente/putCliente', cliente, {params: {id:cliente.id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const deletaCliente = (id:number) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.delete('api/cliente/deleteCliente', {params: {id:id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const salvarCliente = (cliente:Cliente) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.post('/api/cliente/postCliente', cliente)
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}