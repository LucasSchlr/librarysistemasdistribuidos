import axios from 'axios'
import { Livro, LivroDto } from '../interfaces/livro/Livro'
import adicionaModalErro from '../components/utils/LayoutErro'

export async function listaLivros():Promise<Array<Livro>> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/livro/getLivro')
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export async function livroPorId(id:number):Promise<Livro> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/livro/getLivroPorId', {params:{id: id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export const alterarLivro = (livro:LivroDto) => {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.put('/api/livro/putLivro', livro, {params: {id:livro.id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const deletaLivro = (id:number) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.delete('api/livro/deleteLivro', {params: {id:id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const salvarLivro = (livro:LivroDto) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.post('/api/livro/postLivro', livro)
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}