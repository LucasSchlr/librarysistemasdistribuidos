import axios from 'axios'
import { Locacao, LocacaoDto } from '../interfaces/locacao/Locacao'
import adicionaModalErro from '../components/utils/LayoutErro'

export async function listaLocacoes():Promise<Array<Locacao>> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/locacao/getLocacao')
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export async function locacaoPorId(id:number):Promise<Locacao> {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.get('/api/locacao/getLocacaoPorId', {params:{id: id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })

}

export const alterarLocacao = (id: number) => {    
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.put('/api/locacao/putLocacao', {}, {params: {id:id}})
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const deletaLocacao = (id:number) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.delete('api/locacao/deleteLocacao')
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}

export const salvarLocacao = (locacao:LocacaoDto) => {
    return new Promise(async (resolve: (data:any) => void, reject: (data:any) => void) => {
        try {
            const response = await axios.post('/api/locacao/postLocacao', locacao)
            resolve(response.data)
        }catch (err:any) {
            adicionaModalErro(err.response.data)
        }
    })
}