import styled from 'styled-components'

export const ScreenContentContainer = styled.div`
   display:flex;
   justify-content:center;   
   align-items:center;
`

export const TableContainer = styled.div`
   width: 80%;
`