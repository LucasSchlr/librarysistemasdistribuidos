import styled from 'styled-components'

export const ScreenLayoutHeader = styled.div`
  padding-top:8px;
  padding-left: 8px;
`

export const ScreenLayoutBody = styled.div`
  display: grid;
  padding-bottom: 8px;
  grid-template-columns: 30fr 70fr;
  height:100%;
`

export const ScreenLayoutFooter = styled.div`
  height:fit-content;
  padding-left: 16px;
  flex-direction: row-reverse;
  display: flex;
  justify-content:space-between;
  align-items:center;
`

export const FieldsFilterLayout = styled.div`
  padding-top:20px;
`

export const DataGridLayout = styled.div`
  overflow-y: auto;
  max-height: 93%;   
  margin-right: 10px;
`

export const EditFilterButton = styled.div`
  display: flex;
  align-items: center;
`

export const DataTableContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  max-height:  100%;    
  min-height:  100%;
`

export const ButtonFooterContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  max-height: 100%;
  margin-right: 10px;
  margin-bottom: 10px;
  margin-top: 10px;
  >div{
    margin-right: 8px;
  }
`

export const ItemEditFiltroContainer = styled.div`
  display: grid;
  grid-template-columns: 15fr 85fr;
  padding-left: 10px;
  padding-right: 15px;
`
export const EditFiltersContainer = styled.div`
  display: flex;  
`

export const EditFiltroContainer = styled.div`
  margin-left: 4px;
  margin-right: 4px;
`

export const LabelCaptionBrowser = styled.div`
  font-weight: bold;
  font-size:1.5em
`