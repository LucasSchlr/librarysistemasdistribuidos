import styled from 'styled-components'

export const EditPesquisaContainer = styled.div`
  display: flex;
`

export const LabelPesquisaLayout = styled.div`
  height: 100%;
  width: fit-content;
  margin-right: 8px;
`

export const EditPesquisaLayout = styled.div`
  height: 100%;
  width: fit-content;
  margin-right: 8px;
`

export const EditImageContainer = styled.div`
  margin-right: 8px;
  display: flex;
`

