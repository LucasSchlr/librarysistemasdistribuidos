import styled from 'styled-components'

export const FormContainer = styled.div`
    display: flex;
    justify-content: center;
    div:nth-child(2) {
        border-left: 1px solid #e9e9e9;
        margin-left:10px;
        padding-left:10px;
    }
`

export const TableContainer = styled.div`
    width:fit-content;
`

export const SolicitaCodigosContainer = styled.div`
    cursor: pointer;
    margin-top:8px;
    align-items: center;
    display:flex;
    width:450px;
    max-width:450px;
    height:35px;   
    > button {
    margin-left: 6px;
    } 
    > label {
    margin-right:6px
    }
    > div {
    margin-left:6px
    }
`