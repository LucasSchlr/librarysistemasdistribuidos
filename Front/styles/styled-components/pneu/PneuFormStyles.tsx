import styled from 'styled-components'

export const CicloTipoLayout = styled.div`
    display:flex;
    justify-content:space-around;
    margin-top:16px;
`

export const  CicloTipoContainer = styled.div`
    width:48%;
`

export const CicloTipoLabelContainer = styled.div`
    display:flex;
    justify-content:space-between;
    font-size:1.3em;
    font-weight:bold;
    margin-bottom:8px;
`

export const CicloTipoFieldsContainer = styled.div`
    width:100%;
    height:fit-content;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    padding:12px;
    border-radius:8px;
    :hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
    margin-bottom:8px;
`