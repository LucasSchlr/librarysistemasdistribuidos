import styled from 'styled-components'

export const SolicitaCodigosContainer = styled.div`
    cursor: pointer;
    margin-top:8px;
    align-items: center;
    display:flex;
    width:450px;
    max-width:450px;
    height:35px;   
    > button {
        margin-left: 6px;
    } 
    > label {
        margin-right:6px
    }
    > div {
        margin-left:6px
    }
`