import styled from 'styled-components'

export const SwitchContainer = styled.div`

    margin-top:8px;
    margin-bottom:8px;
    height:fit-content;
    width:fit-content;

    > span {
        margin-left:8px
    }
`