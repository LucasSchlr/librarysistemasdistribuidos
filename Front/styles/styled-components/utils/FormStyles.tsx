import styled from 'styled-components'

export const IDTextContainer = styled.div`
   width:fit-content;
   display:flex;
`

export const ControlButtonsContainer = styled.div`
    justify-content: end;
    display:flex;
    > div {
        margin-left:8px;
        margin-top:8px;
    }
`

const BotaoInsertUpdate = styled.div`
    cursor: pointer;
    width:28px;
    height:28px;
    font-weight:bold;
    border-radius:30px;
    display: flex;
    justify-content: center;
    align-items: center;    
`

export const BotaoInsert = styled(BotaoInsertUpdate)`
    border: 3px solid #3770b1;
    color: #3770b1;
`

export const BotaoDelete = styled(BotaoInsertUpdate)`
    border: 3px solid #ff0000;
    color: #ff0000;
    /* border: 3px solid #adacac; */
`

const BotaoControle = styled.div`
    border-radius:14px;
    width:28px;
    min-width:100px;
    height:fit-content;
    font-size:17px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding-left:8px;
    padding-right:8px;
    cursor: pointer;
    :hover {
        box-shadow: 0 6px 6px 0 rgba(0,0,0,0.2);
    }
`

export const BotaoSalvar = styled(BotaoControle)`
    background-color:#3770b1;
    color:white;
    :hover {
        background-color:#255c9b;
    }
`

export const BotaoCancelar = styled(BotaoControle)  `
    background-color:#f8f9fa;
    color:#969494;
    border: 1px solid #adacac;
    :hover {
        background-color:#ebebeb;
        box-shadow: 0 6px 6px 0 rgba(0,0,0,0.2);
    }
`

export const BotaoAdicionarCodigos = styled(BotaoSalvar)`
    width:fit-content ;
`