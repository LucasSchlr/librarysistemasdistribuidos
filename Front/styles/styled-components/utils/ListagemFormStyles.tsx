import styled from 'styled-components'

export const ControlButtonsContainer = styled.div`
   cursor: pointer;
   margin-top:8px;
   align-items: center;
   display:flex;
   width:100%;
   height:35px; 
   justify-content:space-between;
   > div {
     margin-left:6px
   }
`

export const EditPesquisaContainer = styled.div`
  display:flex;
   > label {
     margin-right:6px
   }
`

export const ActionButtonsContainer = styled.div`
   cursor: pointer;
   display:flex;
   margin-left: 10px;
   width:60%;
   max-width:450px;
   justify-content:space-between;
`

export const ActionButtonsContainerLista = styled.div`
  cursor: pointer;
  display:flex;
  width:68px;
  >span{
    margin-left: 10px;
  }
`