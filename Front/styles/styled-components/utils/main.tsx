import styled from 'styled-components'

export const PageContainer = styled.div`  
  background-color:#f8f9fa;
  position:absolute;
  top:0px;
  right:0px;
  bottom:0px;
  left:0px;
`
export const HeaderContainer = styled.div`
   background-color:#3770b1; 
   color: #FFFFFF;
   font-weight: bold;
`

export const ButtonLoginContainer = styled.div`
   display: flex;
   height: fit-content;
   justify-content:center;
   align-items: center;
   background-color:#6c757da0; 
   margin-right: 100px ;
   padding:5px 5px 5px 20px;
   border-radius:10px;
`

export const ScreenTitleContainer = styled.div`
   border-bottom: 1px solid #e9e9e9;
   margin-bottom: 10px;
   height:40px;
   display: flex;
   justify-content:center;
   align-items: center;
   font-size:1.6em;
   font-weight:bold;
`
export const MainContent = styled.div`
   height: fit-content;
   display:flex;
   justify-content:center;
`

export const MainContentContainer = styled.div`
   height: 10%;
   margin-top:12px;   
   width:80%;
`

export const MainContentCardContainer = styled.div`
   background-color: white;
   height:fit-content;
   width:100%;
   box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
   transition: 0.3s;
   padding:12px;
   border-radius:8px;
   :hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
   }
`

export const ImageButtonContainer = styled.div`
   cursor: pointer;
`

export const ErrorDiv = styled.div`
   background-color:#ffb3bf;
   border-radius: 3px;
   padding: 8px;
`
export const ErrorDivDate = styled.div`
   font-size: 14px;
   text-align: right;
`