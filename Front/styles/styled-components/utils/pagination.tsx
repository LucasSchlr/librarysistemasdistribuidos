import styled from 'styled-components'



export const PaginacaoControllerContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    height:30px;
    font-size:15px;    
    margin-top:10px;
    padding: 10px;
    margin-bottom: 10px;
    border-top: 1px solid #e9e9e9;
`

export const EditItensPaginaContainer = styled.div`
    display: flex;
    margin-right:auto;
    height:10px;
    > div {
        margin-right:8px;
        margin-left:8px;
    }
`

export const EditItensPagina = styled.div`
    width:40px;
`

export const ControlesPaginacao = styled.div`
    display: flex;
    flex-direction: row;
    margin-right:50px;
    > div{
        margin:2px
    }
`

export const PaginationNumber = styled.div`
    cursor: pointer;
    font-size:18px;
    width:30px;
    height:30px;
    text-align:center;
    opacity:0.5;
    padding:2px;
`

export const SelectedPaginationNumber = styled(PaginationNumber)`
    opacity:1;
    font-weight:bold;
`

export const BotaoPrimeiroUltimo = styled.div`
    cursor: pointer;
    justify-content:center;
    align-items:center;
    display: flex;
    width:30px;
    height:30px;
`